load 'lib/board.rb'
load 'lib/player.rb'

class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(pos)
    @board[pos] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    @board.display
    attack(@player.get_play)
  end

  def play
    p 'Welcome to Battleship!'
    @board.place_random_ship if @board.empty?
    play_turn until game_over?
    p 'You win!'
  end
end

ng = BattleshipGame.new(HumanPlayer.new, Board.new)
ng.play
