class HumanPlayer
  def get_play
    p 'Target which row?'
    row_tar = gets.chomp

    p 'Target which column?'
    column_tar = gets.chomp

    [row_tar.to_i, column_tar.to_i]
  end
end
