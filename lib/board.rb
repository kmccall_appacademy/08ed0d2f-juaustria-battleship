class Board
  attr_accessor :grid

  def initialize(grid = nil)
    @grid = if grid.nil?
              Board.default_grid
            else
              grid
            end
  end

  def self.default_grid
    @grid = Array.new(10).map { |_row| Array.new(10) }
  end

  def display
    @grid.each { |row| p row } 
  end

  def empty?(pos = @grid)
    if pos == @grid
      @grid.each do |row|
        row.each do |pos|
          return false if pos == :s
        end
      end
      true
    else
      @grid[pos[0]][pos[1]].nil?
    end
  end

  def full?
    @grid.each do |row|
      row.each do |pos|
        return false if pos.nil?
      end
    end
    true
  end

  def place_random_ship
    raise 'The board is full' if full?
    pos1 = rand(@grid.length)
    pos2 = rand(@grid.length)
    @grid[pos1][pos2] = :s if @grid[pos1][pos2].nil?
  end

  def count
    ship_count = 0
    @grid.each do |row|
      row.each do |pos|
        ship_count += 1 if pos == :s
      end
    end
    #### Variable ship length
    # found_ship = false
    # @grid.each_with_index do |row, idx|
    #   @grid.each_with_index do |pos, idx2|
    #     if pos == :s && found_ship == false
    #       found_ship == true
    #     end
    #   end
    # end
    ship_count
  end

  def won?
    @grid.each do |row|
      row.each { |pos| return false if pos == :s }
    end
    true
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end
end
